var active_image = 0;

function keyControl(evt, lightboxID){
    var evt = evt || window.evt;
    var keyCode = evt.keyCode || evt.which;
    if(keyCode == 37){
        previousImage(lightboxID);
        console.log("previous");
    }
    else if(keyCode == 39){
        nextImage(lightboxID);
        console.log("next");
    }
    else if(keyCode == 27){
        closeLightbox(lightboxID);
        console.log("close");
    }
}

function nextImage(lightboxID){
    var images = document.querySelectorAll(lightboxID+" .lightbox-gallery .img");
    var new_index = active_image == images.length - 1 ? 0 : active_image + 1;
    showImage(lightboxID, images[new_index].dataset.src.replace('thumbnails','fullHD'), new_index);
    selectImageInGallery(lightboxID, new_index);
}
function previousImage(lightboxID){
    var images = document.querySelectorAll(lightboxID+" .lightbox-gallery .img");
    var new_index = active_image == 0 ? images.length - 1 : active_image - 1;
    showImage(lightboxID, images[new_index].dataset.src.replace('thumbnails','fullHD'), new_index);
    selectImageInGallery(lightboxID, new_index);
}
var eventHandler;
function closeLightbox(lightboxID){
    document.querySelector(lightboxID).classList.remove("active");
    window.removeEventListener("keyup", eventHandler);
}

function showLightbox(lightboxID, src, index){
    document.querySelector(lightboxID).classList.add('active');
    selectLightboxImage(lightboxID, src, index);
    eventHandler = function(e){
        keyControl(e, lightboxID);
    }
    window.addEventListener("keyup", eventHandler);
}
function selectLightboxImage(lightboxID, src, index){
    showImage(lightboxID, src, index);
    selectImageInGallery(lightboxID, index);
}
function showImage(lightboxID, src, index){
    document.querySelector(lightboxID + " .full").src = src.replace('thumbnails','fullHD');
    active_image = index;
    selectImageInGallery(lightboxID, index);
}
var active_index = 0;
function selectImageInGallery(lightboxID, index){
    active_index = index;
    document.querySelector(lightboxID+" .lightbox-gallery .img.active").classList.remove("active");
    var images = document.querySelectorAll(lightboxID+" .lightbox-gallery .img");
    images[index].classList.add("active");
    var left = parseInt(window.getComputedStyle(document.querySelector(lightboxID+" .lightbox-gallery-slider")).left.replace("px",""));
    if(180*index > left+window.innerWidth/2 && index)
        document.querySelector(lightboxID+" .lightbox-gallery-slider").style.left = (-180*index)+window.innerWidth/2+"px";
    else
        document.querySelector(lightboxID+" .lightbox-gallery-slider").style.left = "0px";
}

window.addEventListener("resize",function(){
    selectImageInGallery("#some-lightbox",active_index);
});