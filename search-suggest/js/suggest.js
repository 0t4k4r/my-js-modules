var suggestions = `{
    "categories" : 
        [
            { "name":"category ABC", "description":"the description of category 01" },
            { "name":"category ACD", "description":"the description of category 02" },
            { "name":"category ACB", "description":"the description of category 03" },
            { "name":"category BCD", "description":"the description of category 04" },
            { "name":"category EFG", "description":"the description of category 05" }
        ],
    "products" : 
        [
            { "id":1, "name":"Product ABD", "image":"./data/img/product-01.jpg", "description":"the description of product 01", "price":990, "currency":"CZK", "url":"./produkt?id=1" },
            { "id":2, "name":"Product DDD", "image":"./data/img/product-02.jpg", "description":"the description of product 02", "price":890, "currency":"CZK", "url":"./produkt?id=2" },
            { "id":3, "name":"Product BAC", "image":"./data/img/product-03.jpg", "description":"the description of product 03", "price":1240, "currency":"CZK", "url":"./produkt?id=3" },
            { "id":4, "name":"Product EBA", "image":"./data/img/product-04.jpg", "description":"the description of product 04", "price":500, "currency":"CZK", "url":"./produkt?id=4" },
            { "id":5, "name":"Product CBG", "image":"./data/img/product-05.jpg", "description":"the description of product 05", "price":0.02, "currency":"BTC", "url":"./produkt?id=5" }
        ]
}`;

function transformData(input){
    input = input.value.toLowerCase();
    var data = JSON.parse(suggestions);
    
    for(var i = data.categories.length-1; i >= 0; i--){
        var item = data.categories[i];
        if(!item.name.toLowerCase().includes(input) && !item.description.toLowerCase().includes(input)){
            data.categories.splice(i,1);
        }
    }
    for(var i = data.products.length-1; i >= 0; i--){
        var item = data.products[i];
        if(!item.name.toLowerCase().includes(input) && !item.description.toLowerCase().includes(input)){
            data.products.splice(i,1);
        }
    }
    return data;
}

function pleaseSuggest(input){
    var data = transformData(input);
    var html = `
    <div id="suggested-categories" class="suggestion-type">
        <h2>Categories</h2>`;
    data.categories.forEach(function(item){
        html += `
        <div class="suggested-item">
            <h3>`+item.name+`</h3>
            <p>`+item.description+`</p>
        </div>`;
    });
    html += `
        <a class="more" href="./categories?query=`+input.value+`">View more categories</a>
    </div>`;
    html += `
    <div id="suggested-products" class="suggestion-type">
        <h2>Products</h2>`;
    data.products.forEach(function(item){
        html += `
        <div class="suggested-item">
            <h3>`+item.name+`</h3>
            <p>`+item.description+`</p>
        </div>`;
    });
    html += `
        <a class="more" href="./categories?query=`+input.value+`">View more products</a>
    </div>`;
    document.querySelector("#search-suggestions-container").innerHTML = html;
}