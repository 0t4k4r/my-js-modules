function getClosestParent(el, selector){
    var parent = el.parentNode;
    var parent_matches = false;
    if(parent && parent != document){
		parent_matches = parent.matches(selector);
        if(parent_matches){
            return parent;
        }else{
            return getClosestParent(parent, selector);
        }
    }else{
        return false;
    }
}

//direction is -1 or +1
function slideInfiniteCarousel(btn, direction){
    // 0) make variables of all important objects
    var carousel = getClosestParent(btn, ".carousel");

    var items = carousel.querySelectorAll(".item");
    var item_classes = [].map.call(items, function(el){
        if(el.classList.contains("active")){
            return "active";
        }else{
            return "";
        }
    });

    var active_item_index = item_classes.indexOf("active");
    var new_item_index = (items.length + active_item_index + direction) % items.length;
    console.log(new_item_index);

    var active_item = carousel.querySelector(".item.active");
    if(!active_item){
        active_item = carousel.querySelector(".item");
    }

    var new_item = items[new_item_index];
    console.log(new_item);

    // 1) place object that it's supposed to slide to in the right direction
    if(direction < 0){
        new_item.style.right = "100%";
        new_item.style.left = "auto";
    }
    // 2) move active and new object
    if(direction < 0){
        new_item.style.transition = "transform .7s ease";
        active_item.style.transition = "transform .7s ease";
    }else{
        new_item.style.transition = "transform .7s ease";
        active_item.style.transition = "transform .7s ease";
    }
    window.setTimeout(function(){
        if(direction < 0){
            new_item.style.transform = "translateX(100%)";
            active_item.style.transform = "translateX(100%)";
        }else{
            new_item.style.transform = "translateX(-100%)";
            active_item.style.transform = "translateX(-100%)";
        }
    },50);
    // 3) status quo
    window.setTimeout(function(){
        new_item.setAttribute("style","");
        new_item.classList.add("active");
        active_item.setAttribute("style","");
        active_item.classList.remove("active");
    },750);
}