var otakar_beinhauers_calendar_data_storage = "";

var current_input_id = "#date-of-birth";

var minYear = 1900;
var maxYear = 2100;

function getDay(y,m,d){
    y = m < 3 ? y - 1 : y;
    m = m == 1 ? 13 : m;
    m = m == 2 ? 14 : m;
    var temp_number = d + 2*m + Math.floor(3*(m+1)/5) + y + Math.floor(y/4) - Math.floor(y/100) + Math.floor(y/400) + 2;
    return (temp_number - 2) % 7;
}
function isItLeapYear(y){
    return y % 4 == 0 && (y % 100 != 0 || y % 400 == 0);
}
function DaysInAMonth(y,m){
    if(m < 1 || m > 12)
        console.error("Error - invalid month was passed: month number "+m+" does not exist.");
    days_in_months = [31,28,31,30,31,30,31,31,30,31,30,31];
    return m == 2 ? (isItLeapYear(y) ? 29 : 28) : days_in_months[m-1];
}

function monthNumberToName(num){
    var month_array = ["January","February","March","April","May","June","July","August","September","October","November","December"];
    return month_array[num];
}

function createNewTBodyElement(y, m){
    var firstDay = getDay(y,m,1);
    var daysCount = DaysInAMonth(y,m);
    var table = document.querySelector("#calendar-container table");
    var tbody = document.createElement("tbody");
    tbody.id = "calendar-container-"+y+"-"+m;
    tbody.classList.add("month");
    var html = "<tr>";
    for(var i = 0; i < firstDay; i++){
        html += "<td></td>";
    }
    for(var i = firstDay; i < daysCount + firstDay; i++){
        html += (i+1) % 7 == 0 && i != daysCount + firstDay - 1 ? 
            "<td onclick='putDate(this,"+(i-firstDay+1)+")'>"+(i-firstDay+1)+"</td></tr>" : "<td onclick='putDate(this,"+(i-firstDay+1)+")'>"+(i-firstDay+1)+"</td>";
    }
    for(var i = daysCount + firstDay - 1; (i+1) % 7 != 0; i++){
        html += "<td></td>";
    }
    html += "</tr>";
    tbody.innerHTML = html;
    table.appendChild(tbody);
}

function putDate(cell, day){
    var current_input = document.querySelector(current_input_id);
    var value = "";
    value += day + ". ";
    value += document.querySelector("#calendar-container .month-selector .month").dateTime.split("-")[1] + ". ";
    value += document.querySelector("#calendar-container .month-selector .year").value;
    current_input.innerHTML = value;
}

function activateTBody(y,m){
    if(document.querySelector("#calendar-container tbody.month.active"))
        document.querySelector("#calendar-container tbody.month.active").classList.remove("active");
    document.querySelector("#calendar-container-"+y+"-"+m).classList.add("active");
}

function renderMonth(y,m){
    y = parseInt(y);
    if(y< minYear)
        y = minYear;
    if(y > maxYear)
        y = maxYear;
    m = parseInt(m);
    if(m < 1)
        m = 1;
    if(m > 12)
        m = 12;
    document.querySelector("#calendar-container .month-selector .month").innerHTML = monthNumberToName(m-1);
    document.querySelector("#calendar-container .month-selector .month").dateTime = y + "-" + (m - 1 < 9 ? "0" + m : m);
    document.querySelector("#calendar-container .month-selector .year").value = y;
    if(!document.querySelector("#calendar-container-"+y+"-"+m)){
        createNewTBodyElement(y, m);
        //renderData(y, m);
    }
    activateTBody(y, m);
}

function calendarInit(){
    var date = new Date();
    var y = date.getFullYear();
    var m = date.getMonth() + 1;
    var d = date.getDate();
    var firstDay = getDay(y,m,1);
    //initialRequestForData(y);
    renderMonth(y, m);
    var todaysCell = document.querySelectorAll("#calendar-container-"+y+"-"+m+" td")[firstDay + d - 1];
    if(~todaysCell.innerHTML.indexOf("span"))
        todaysCell.querySelector("span").classList.add("today");
    else
        todaysCell.innerHTML = "<span class='today'>"+todaysCell.innerHTML+"</span>";
}

function openDatePicker(picked){
    var picked_date = picked.innerHTML;
    current_input_id = "#"+picked.id;
    if(picked_date){
        var splitted_date = picked_date.split(". ");
        var year = splitted_date[2];
        var month = splitted_date[1];
        renderMonth(year, month);
        document.querySelector("#datepicker-container").classList.add("active");
    }else{
        document.querySelector("#datepicker-container").classList.add("active");
    }
    document.querySelector("#datepicker-container").addEventListener("click",function f(event){
        console.log(event.target);
        if(!this.querySelector("#calendar-container").contains(event.target) || event.target.tagName == "TD"){
            this.classList.remove("active");
            document.removeEventListener('click',f);
        }
    });
}

window.addEventListener("load",calendarInit);

function nextMonth(){
    var date = document.querySelector("#calendar-container tbody.month.active").id.replace("calendar-container-","").split("-");
    var y = parseInt(date[0]);
    var m = parseInt(date[1]);
    if(m == 12){
        y++;
        m = 1;
    }else{
        m++;
    }
    var d = new Date();
    if(y > d.getFullYear()+1){
        //getThisMonthData(y, m);
    }
    renderMonth(y, m);
}

function previousMonth(){
    var date = document.querySelector("#calendar-container tbody.month.active").id.replace("calendar-container-","").split("-");
    var y = date[0];
    var m = date[1];
    if(m == 1){
        y--;
        m = 12;
    }else{
        m--;
    }
    var d = new Date();
    if(y < d.getFullYear() - 1){
        //getThisMonthData(y, m);
    }
    renderMonth(y, m);
}

function yearInput(year){
    if(year.value >= minYear){
        renderMonth(year.value, document.querySelector("#calendar-container .month-selector .month").dateTime.split("-")[1]);
        year.classList.remove("off-limit");
    }
    else{
        year.classList.add("off-limit");
    }
}