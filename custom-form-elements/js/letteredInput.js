window.addEventListener("load",function(){
    var letters = document.querySelectorAll(".letter input");
    for(var i = 0; i < letters.length; i++){
        var letter = letters[i];
        letter.onfocus = function(event){
            var input = event.target;
            input.dataset.oldValue = input.value;
        }
        letter.onkeyup = function(event){
            var input = event.target;

            if(event.key == "ArrowLeft"){
                if(input.parentNode.previousElementSibling){
                    input.parentNode.previousElementSibling.querySelector("input").focus();
                }
            }
            if(event.key == "ArrowRight"){
                if(input.parentNode.nextElementSibling){
                    input.parentNode.nextElementSibling.querySelector("input").focus();
                }
            }
            if(event.key == "Backspace"){
                if(!input.dataset.oldValue){
                    var prev = input.parentNode.previousElementSibling;
                    while(prev){
                        var prev_input = prev.querySelector("input");
                        if(prev_input.value){
                            prev_input.value = "";
                            break;
                        }
                        if(prev.previousElementSibling){
                            prev = prev.previousElementSibling;
                        }else{
                            break;
                        }
                    }
                    if(prev.previousElementSibling){
                        prev.previousElementSibling.querySelector("input").focus();
                    }else{
                        prev.querySelector("input").focus();
                    }
                }
            }
        }
        letter.oninput = function(event){

            var input = event.target;

            input.value = input.value.replace(input.dataset.oldValue,"");
            if(input.value){
                input.value = input.value[0];
            }
            input.dataset.oldValue = input.value;

            if(input.value){
                if(input.parentNode.nextElementSibling){
                    input.parentNode.nextElementSibling.querySelector("input").focus();
                }
            }else{
                if(input.parentNode.previousElementSibling){
                    input.parentNode.previousElementSibling.querySelector("input").focus(function(event){event.target.select();});
                }
            }

            var letters = input.parentNode.parentNode.querySelectorAll(".letter input");
            var word = "";
            for(var l = 0; l < letters.length; l++){
                word += letters[l].value;
            }
            input.parentNode.parentNode.parentNode.querySelector(".input").value = word;
        };
    }
});