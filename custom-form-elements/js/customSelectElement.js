function toggleSelectOptionsVisibility(element){
    var index;
    if(element.querySelector(".options").className == "options active"){
        element.querySelector(".options").className = "options";
    }
    else{
        var options_array = element.querySelectorAll(".options > *");
        var height = 0;
		for(var i = 0; i < options_array.length; i++) {
			var option = options_array[i];
            height += parseInt(getComputedStyle(option).height.replace("px",""));
        }
        for(var i = 0; i < options_array.length; i++) {
			var option = options_array[i];
            option.addEventListener( 'click', function optionClick(event){
                for(var ii = 0; ii < options_array.length; ii++) {
					var namespace_1_element = options_array[ii];
                    namespace_1_element.className = "option";
                }
            event.target.className = "option active";
            });
        }		
        element.querySelector(".options").className = "options active";
        element.querySelector(".options").style.height = height + "px";
        document.addEventListener( 'click', function f(event) {
            document.removeEventListener( 'click', f);
            document.addEventListener( 'click', function f(event) {
                var active_options = element.querySelector(".options");
                if(!active_options.contains(event.target)){
                    element.querySelector(".options").className = "options";
                    document.removeEventListener('click',f);
                }
                else{
                    element.querySelector("input").value = event.target.querySelector(".option-value").innerHTML;
                    element.querySelector(".options").className = "options";
                    element.querySelector(".placeholder").className = "placeholder";
                    element.querySelector(".final-value").innerHTML = event.target.childNodes[0].nodeValue;
                    element.querySelector(".final-value").className = "final-value active";
                    document.removeEventListener('click',f);
                }
            });
        });
    }
    
}