function openTreePicker(input_el){
    var id = "#tree-id";
    var tree = document.querySelector(id);
    
    tree.querySelector(".input-container button").dataset.inputId = input_el.id;
    setTimeout(function(){
        tree.classList.add("active");
        window.addEventListener("click",closeTreeEventHandler);
    },50);
}

function closeTree(){
    var tree = document.querySelector(".tree.active");
    tree.classList.remove("active");
    window.removeEventListener("click",closeTreeEventHandler);
}
function closeTreeEventHandler(e){
    var tree = document.querySelector(".tree.active");
    if(!tree.innerHTML.includes(e.target.innerHTML)){
        closeTree();
    }
}

function expandBranch(name_el){
    var parent = name_el.parentNode;
    if(!parent.classList.contains("leaf")){
        parent.classList.toggle("open");
    }
    var path = name_el.innerHTML;
    var i = 0;
    while(parent.parentNode.parentNode.querySelector(".name") == parent.parentNode.parentNode.children[0]){
        path = parent.parentNode.parentNode.querySelector(".name").innerHTML + "/" + path;
        parent = parent.parentNode.parentNode;
        if(i > 20)
            break;
        i++;
    }
    parent.parentNode.parentNode.querySelector(".input-container .tree-input").value = path;
    if(parent.parentNode.parentNode.querySelector(".active"))
        parent.parentNode.parentNode.querySelector(".active").classList.remove("active");
    name_el.parentNode.classList.add("active");
}

function pickTreePath(btn){
    var result = btn.parentNode.querySelector(".tree-input").value;
    var chosen_field = document.querySelector("#"+btn.dataset.inputId+" .chosen");
    var placeholder = document.querySelector("#"+btn.dataset.inputId+" .placeholder");
    chosen_field.value = result;
    placeholder.classList.remove("active");
    chosen_field.classList.add("active");
    closeTree();
}

function emptyChosenTreeValue(btn){
    var chosen_field = btn.parentNode.querySelector(".chosen");
    var placeholder = btn.parentNode.querySelector(".placeholder");
    chosen_field.value = "";
    chosen_field.classList.remove("active");
    placeholder.classList.add("active");
}

window.addEventListener("load",function(){
    if(document.querySelector(".tree .name")){
        var names = document.querySelectorAll(".tree .name");
        for(var i = 0; i < names.length; i++){
            var name = names[i];
            (function(name){
                var eventHandler = function(){
                    expandBranch(name);
                }
                name.addEventListener("click",eventHandler);
            })(names[i]);
            
        }
    }
});
