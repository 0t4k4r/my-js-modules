var button;
var currentLeft;
var cursorX;
var range_width;
var double_range_min = 0;
var double_range_max = 0;

var drag = function(e){
    var posX = e.pageX || e.clientX;
    posX = (posX - cursorX);
    var left = currentLeft+posX > 190 ? 190 : (currentLeft+posX < -10 ? -10 : currentLeft+posX);
    button.style.left = left+"px";
    changeMinMaxValue();
}
var addDoubleRangeButtonMove = function(e){
    cursorX = e.pageX || clientX;
    range_length = parseInt(window.getComputedStyle(button.parentNode).width.replace("px",""));
    currentLeft = parseInt(window.getComputedStyle(button).left.replace("px",""));
    window.addEventListener("mousemove", drag);
    window.addEventListener("mouseup", removeDoubleRangeButtonMove);
}
var removeDoubleRangeButtonMove = function(e){
    window.removeEventListener("mouseup", removeDoubleRangeButtonMove);
    window.removeEventListener("mousemove", drag);
    cursorX = 0;
    button = 0;
}
function changeMinMaxValue(){
    var form_element = button.parentNode.parentNode;
    var button1 = form_element.querySelectorAll(".double-range-range button")[0];
    var button2 = form_element.querySelectorAll(".double-range-range button")[1];
	var price_filter1 = parseInt(window.getComputedStyle(button1).left.replace("px",""))+10;
	var price_filter2 = parseInt(window.getComputedStyle(button2).left.replace("px",""))+10;
	var minimum = price_filter1 <= price_filter2 ? price_filter1 : price_filter2;
    var maximum = price_filter1 > price_filter2 ? price_filter1 : price_filter2;
	form_element.querySelector(".double-range-from-input").value = Math.floor((minimum / range_width) * double_range_max);
    form_element.querySelector(".double-range-to-input").value = Math.floor((maximum / range_width) * double_range_max);
}

function changeMinMaxSlider(el, index){
    var parent = el.parentNode;
	var other_index = index ? 0 : 1;
	if(parseInt(el.value) > el.max){
		el.value = el.max;
	}
	if(el.value != "" && parseInt(el.value) < el.min){
		el.value = el.min;
    }

    var button1 = parent.querySelectorAll(".double-range-range button")[0];
    var button2 = parent.querySelectorAll(".double-range-range button")[1];
    var price_filter1 = parseInt(window.getComputedStyle(button1).left.replace("px",""))+10;
    var price_filter2 = parseInt(window.getComputedStyle(button2).left.replace("px",""))+10;
    var min_button = price_filter1 <= price_filter2 ? button1 : button2;
    var max_button = price_filter1 > price_filter2 ? button1 : button2;
	var other = parent.querySelectorAll("input")[other_index];
	if(parseInt(el.value) <= parseInt(other.value)){
        min_button.style.left = (Math.floor((parseInt(el.value) / double_range_max) * range_width)-9) + "px";
        max_button.style.left = (Math.floor((parseInt(other.value) / double_range_max) * range_width)-9) + "px";
	}
	else{
        max_button.style.left = (Math.floor((parseInt(el.value) / double_range_max) * range_width)-9) + "px";
        min_button.style.left = (Math.floor((parseInt(other.value) / double_range_max) * range_width)-9) + "px";
	}
}

window.addEventListener("load",function(){
    range_width = parseInt(window.getComputedStyle(document.querySelector(".double-range-range")).width.replace("px",""));
    var double_range_element = document.querySelector(".custom-form-element.double-range");
    var double_range_buttons = double_range_element.querySelectorAll(".custom-form-element.double-range button");
    var double_range_inputs = double_range_element.querySelectorAll("input");
    double_range_min = parseInt(double_range_inputs[0].min);
    double_range_max = parseInt(double_range_inputs[0].max);
    for(var i = 0; i < double_range_buttons.length; i++){
        var el = double_range_buttons[i];
        (function (el){
            el.addEventListener("mousedown", function(e){
                button = el;
                addDoubleRangeButtonMove(e);
            });
        })(el);
    }
    var input_index = 0;
    for(var i = 0; i < double_range_inputs.length; i++){
        var el = double_range_inputs[i];
        (function (el){
            changeMinMaxSlider(el, input_index);
            input_index++;
        })(el);
    }
});