window.addEventListener("load",function(){
    var custom_num_inputs = document.querySelectorAll(".custom-number-container");
})

function isNumberInputValid(input){
    var patt = /\D/g;
    var min = 1;
    var value = input.value.replace(/ /g,"");
    if(value && !patt.test(value) && parseInt(value) >= 0){
        input.classList.remove("invalid");
        return true;
    }
    else{
        input.classList.add("invalid");
        return false;
    }
}

function addOneOfAThing(button){
    var parent = button.parentNode;
    var input = parent.querySelector("input");
    var value = parseInt(input.value.replace(/ /g,""));
    if(isNumberInputValid(input)){
        input.value = value + 1;
    }
    else{
        input.value = 0;
    }
    isNumberInputValid(input);
}

function subtractOneOfAThing(button){
    var parent = button.parentNode;
    var input = parent.querySelector("input");
    var value = parseInt(input.value.replace(/ /g,""));
    if(isNumberInputValid(input) && value != 0){
        input.value = value - 1;
    }
    else{
        input.value = 0;
    }
    isNumberInputValid(input);
}