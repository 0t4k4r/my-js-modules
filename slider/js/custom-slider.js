var vars = {
    "slider" : document.querySelector("#custom-slider"),
    "container" : document.querySelector("#container-of-slides"),
    "infinite" : false,  //true not supported yet
    "slides-share-width" : true, //false not fully supported yet
    "slides-visible" : 1,
    "slides-per-sliding" : 1, //more than one not supported yet
    "first-visible" : 0, //index
    "last-visible" : 0,
    "slide-count" : false,
}

function initCustomSlider(){
    setSliderWidth();
    vars["slide-count"] = vars["slider"].querySelectorAll(" .slide").length;
}
initCustomSlider();

function setSliderWidth(){
    if(vars["slides-share-width"]){
        vars["slides-share-width"] = parseInt(window.getComputedStyle(vars["container"].querySelector(" .slide")).width.replace("px",""));
        vars["slider"].style.width = vars["slides-share-width"] * vars["slides-visible"]+"px";
    }
}

function slideByAmount(int_amount){
    var current_left = parseInt(vars["container"].dataset.leftPosition);
    vars["container"].dataset.leftPosition = current_left + int_amount;
    vars["container"].style.left = (current_left + int_amount) + "px";
}

function nextSlide(){
    var slide_index = (vars["last-visible"]+vars["slides-per-sliding"]) % vars["slide-count"];
    slideToSlide(slide_index);
}
function prevSlide(){
    console.log(vars["first-visible"]);
    console.log(vars["last-visible"]);
    var slide_index = vars["slide-count"]-1 - ((vars["slide-count"]-1 - vars["first-visible"]+vars["slides-per-sliding"]) % vars["slide-count"]);
    console.log(slide_index);
    slide_index -= slide_index > vars["slide-count"]-1 - (vars["slides-visible"]-1) ? vars["slides-visible"] - 1 : 0;
    console.log(slide_index);
    slideToSlide(slide_index);
}

function slideToSlide(index){
    if(index < vars["first-visible"] || index > vars["last-visible"]){
        var sliding_count = 0;
        var slide_length = 0;
        if(index < vars["first-visible"]){
            sliding_count = vars["first-visible"] - index;
            for(i = vars["first-visible"]; i > index; i--){
                slide_length += parseInt(window.getComputedStyle(vars["slider"].querySelectorAll(" .slide")[i]).width.replace("px",""));
            }
            vars["last-visible"] = index + vars["slides-visible"] - 1;
            vars["first-visible"] = index;
        }else{
            sliding_count = index - vars["last-visible"];
            for(i = vars["last-visible"]; i < index; i++){
                slide_length -= parseInt(window.getComputedStyle(vars["slider"].querySelectorAll(" .slide")[i]).width.replace("px",""));
            }
            vars["last-visible"] = index;
            vars["first-visible"] = index - vars["slides-visible"] + 1;
        }
        slideByAmount(slide_length);
    }
}