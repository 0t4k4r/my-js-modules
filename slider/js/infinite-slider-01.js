var infinite_01_active_slide = 0;

var infinite_slider_01 = document.querySelector("#infinite-slider-01");
var infinite_01_left_slides = document.querySelectorAll("#infinite-slider-01 .left .slide");
var infinite_01_right_slides = document.querySelectorAll("#infinite-slider-01 .right .slide");

function nextInfiniteSlide(){
    var visible = infinite_slider_01.querySelectorAll(".visible");
    for(var i = 0; i < visible.length; i++){
        visible[i].classList.remove("visible");
    }
    infinite_01_left_slides[infinite_01_active_slide].classList.add("visible");
    infinite_01_left_slides[infinite_01_active_slide].classList.remove("active");
    infinite_01_right_slides[infinite_01_active_slide].classList.remove("active");
    infinite_01_active_slide = (infinite_01_active_slide + 1) % infinite_01_left_slides.length;
    infinite_01_left_slides[infinite_01_active_slide].classList.add("active");
    infinite_01_right_slides[infinite_01_active_slide].classList.add("active");
    infinite_01_right_slides[infinite_01_active_slide].classList.add("visible");
}