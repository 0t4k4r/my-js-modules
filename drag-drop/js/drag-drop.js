var mouse_down_countdown_func_reference = null;
var mouse_down_timeout = null;

window.addEventListener("load",function(){
    var tiles = document.querySelectorAll(".drag-drop-grid .tile");
    for(var i = 0; i < tiles.length; i++){
        var tile = tiles[i];
        tile.dataset.width = window.getComputedStyle(tile).width;
        tile.dataset.height = window.getComputedStyle(tile).height;
        mouse_down_countdown_func_reference = mouseDownCountdown.bind(null,tileDragBegin,1000);
        tile.addEventListener("mousedown",mouse_down_countdown_func_reference);
    }
});


Element.prototype.remove = function() {
    this.parentElement.removeChild(this);
}

function getEmptySpaces(grid,width,height){
    var result = [];
    for(var row = 0; row < grid.length; row++){
        for(var col = 0; col < grid[row].length; col++){
            var je_volno = true;
            if(grid[row][col]){
                je_volno = false;
            }
            else{
                for(var x = row+1; x < row+width; x++){
                    if(typeof grid[x] !== "undefined"){
                        if(grid[x][col]){
                            je_volno = false;
                        }
                    }
                }
                for(var y = col+1; y < col+height; y++){
                    if(typeof grid[row][y] !== "undefined"){
                        if(grid[row][y]){
                            je_volno = false;
                        }
                    }else{
                        je_volno = false;
                    }
                }
            }
            if(je_volno){
                result.push({"row":row,"col":col});
            }
        }
    }
    return result;
}

//expanding means once there is wider element it extends the considered columns
//is recursive
function getTilesInExpandingColumn(grid_el, tile){
    var result = [];
    var tiles = grid_el.querySelectorAll(".tile");

    var column = parseInt(window.getComputedStyle(sibling).gridColumnStart);
    var column_span = String(window.getComputedStyle(sibling).gridColumnEnd);
    column_span = parseInt(col_span.replace("span ",""));

    for(var c = column; c < column + column_span; c++){
        for(var i = 0; i < tiles.length; i++) {
            if(tiles[i] != tile){
                var sibling = tiles[i];
                var col = parseInt(window.getComputedStyle(sibling).gridColumnStart);
                var col_span = String(window.getComputedStyle(sibling).gridColumnEnd);
                col_span = parseInt(col_span.replace("span ",""));
                if(col >= column && col < column+col_span){
                    result.push(tile);
                }
            }
        }
    }
}

var tile_drag_last_position_x = 0;
var tile_drag_last_position_y = 0;
var tile_drag_func_reference = null;
var tile_drag_end_func_reference = null;

function mouseDownCountdown(fn, time){
    var tile = event.currentTarget;
    fn = fn.bind(null,event,tile);
    mouse_down_timeout = setTimeout(fn,time);
    window.addEventListener("mouseup",mouseDownCancel);
}
function mouseDownCancel(){
    clearTimeout(mouse_down_timeout);
    mouse_down_timeout = null;
    window.removeEventListener("mouseup",mouseDownCancel);
}

var tile_relative_mouseX = 0;
var tile_relative_mouseY = 0;

function tileDragBegin(event,tile){
    
    var rect = tile.getBoundingClientRect();
    tile_relative_mouseX = event.clientX - rect.left;
    tile_relative_mouseY = event.clientY - rect.top;

    /*var grabbed = tile.cloneNode(true);
    grabbed.classList.add("grabbed");
    tile.parentNode.appendChild(grabbed);
    tile = grabbed;*/

    tile.classList.add("grabbed");

    var grid_el = tile.parentNode;
    grid_el.classList.add("being-modified");

    var grid_row_count = window.getComputedStyle(grid_el).gridTemplateRows.split(" ").length;
    var grid_col_count = window.getComputedStyle(grid_el).gridTemplateColumns.split(" ").length;
    
    var grid = new Array(grid_row_count).fill(0).map(() => new Array(grid_col_count).fill(false));

    var tiles = grid_el.querySelectorAll(".tile");
    for (var i = 0; i < tiles.length; i++) {
        if(tiles[i] != tile){
            var sibling = tiles[i];
            var row = parseInt(window.getComputedStyle(sibling).gridRowStart);
            var col = parseInt(window.getComputedStyle(sibling).gridColumnStart);
            var row_span = String(window.getComputedStyle(sibling).gridRowEnd);
            var col_span = String(window.getComputedStyle(sibling).gridColumnEnd);
            row_span = parseInt(row_span.replace("span ",""));
            col_span = parseInt(col_span.replace("span ",""));
            //console.log("row: "+row+", col: "+col+", row_span: "+row_span+", col_span: "+col_span);
            for(var curr_row = row; curr_row < row+row_span; curr_row++){
                for(var curr_col = col; curr_col < col+col_span; curr_col++){
                    //console.log("curr_row: "+curr_row+", curr_col: "+curr_col);
                    grid[curr_row-1][curr_col-1] = true;
                }
            }
        }
    }
    //console.log(grid);
    var row_span = String(window.getComputedStyle(tile).gridRowEnd);
    var col_span = String(window.getComputedStyle(tile).gridColumnEnd);
    row_span = parseInt(row_span.replace("span ",""));
    col_span = parseInt(col_span.replace("span ",""));
    console.log(getEmptySpaces(grid,row_span,col_span));

    tile.style.width = tile.dataset.width;
    tile.style.height = tile.dataset.height;
    tile.style.left = (event.clientX - tile_relative_mouseX) + "px";
    tile.style.top = (event.clientY - tile_relative_mouseY) + "px";

    tile_drag_last_position_x = event.clientX;
    tile_drag_last_position_y = event.clientY;

    tile_drag_func_reference = tileDrag.bind(null, tile);
    tile_drag_end_func_reference = tileDragEnd.bind(null, tile);

    window.addEventListener("mousemove",tile_drag_func_reference);
    window.addEventListener("mouseup",tile_drag_end_func_reference);
}

function tileDrag(tile, event){

    tile.style.left = (event.clientX - tile_relative_mouseX) + "px";
    tile.style.top = (event.clientY - tile_relative_mouseY) + "px";
    /*var mouse_current_position_x = event.clientX;
    var mouse_current_position_x = event.clientY;
    var delta_x = mouse_current_position_x - tile_drag_last_position_x;
    var delta_y = mouse_current_position_y - tile_drag_last_position_y;
    tile_drag_last_position_x = mouse_current_position_x;
    tile_drag_last_position_y = mouse_current_position_y;*/

}


function tileDragEnd(tile){
    var tiles = tile.parentNode.querySelectorAll(".tile");
    var tile_rect = tile.getBoundingClientRect();

    var top_left_collisions = [];
    var top_right_collisions = [];
    var bottom_left_collisions = [];
    var bottom_right_collisions = [];

    for(var i = 0; i < tiles.length; i++){
        if(tile[i] != tile){
            var sibling = tiles[i];
            var sibling_rect = sibling.getBoundingClientRect();
            if(tile_rect.top > sibling_rect.top && tile_rect.top < sibling_rect.bottom && tile_rect.left > sibling_rect.left && tile_rect.left < sibling_rect.right){
                top_left_collisions.push(sibling);
            }
            if(tile_rect.top > sibling_rect.top && tile_rect.top < sibling_rect.bottom && tile_rect.right > sibling_rect.left && tile_rect.right < sibling_rect.right){
                top_right_collisions.push(sibling);
            }
            if(tile_rect.bottom > sibling_rect.top && tile_rect.bottom < sibling_rect.bottom && tile_rect.left > sibling_rect.left && tile_rect.left < sibling_rect.right){
                bottom_left_collisions.push(sibling);
            }
            if(tile_rect.bottom > sibling_rect.top && tile_rect.bottom < sibling_rect.bottom && tile_rect.right > sibling_rect.left && tile_rect.right < sibling_rect.right){
                bottom_right_collisions.push(sibling);
            }
        }
    }

    /*console.log(top_left_collisions);
    console.log(top_right_collisions);
    console.log(bottom_left_collisions);
    console.log(bottom_right_collisions);*/

    //tile.remove();

    /*tile.classList.remove("grabbed");
    tile.style.left = 0;
    tile.style.top = 0;*/

    var grid_el = tile.parentNode;
    grid_el.classList.remove("being-modified");

    window.removeEventListener("mousemove",tile_drag_func_reference);
    window.removeEventListener("mouseup",tile_drag_end_func_reference);
}