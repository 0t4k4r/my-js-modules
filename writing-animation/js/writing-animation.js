var auto_writing_timeout;
var auto_writing_container;
var highlight_timeout;
var highlight;

function writeThis(button){
    clearTimeout(auto_writing_timeout);
    clearTimeout(highlight);
    auto_writing_container = document.querySelector(".auto-writing-input .no-highlight");
    highlight = document.querySelector(".auto-writing-input .highlight");
    var old_s = auto_writing_container.innerHTML;
    deleteText(old_s);
    var new_s = button.innerHTML;
    rewrite(new_s);
}

function deleteText(s){
    console.log("delete");
    if(s){
        auto_writing_container.innerHTML = s.substr(0,s.length-1);
        highlight.innerHTML = s[s.length-1] + highlight.innerHTML;
        s = auto_writing_container.innerHTML;
        var pause = Math.ceil(Math.random()*5)+10;
        highlight_timeout = setTimeout(function(){
            deleteText(s);
        },pause);
    }else{
        setTimeout(function(){
            highlight.innerHTML = "";
        },200);
    }
}

function rewrite(s){
    console.log("rewrite");
    if(s){
        var pause;
        if(highlight.innerHTML == ""){
            auto_writing_container.innerHTML += s[0];
            s = s.substr(1,s.length-1);
            if(s[0] == " "){
                var coin_flip = Math.random();
                if(coin_flip < 0.33){
                    pause = Math.ceil(Math.random()*250)+600;
                }else{
                    pause = Math.ceil(Math.random()*100)+220;
                }
                auto_writing_container.classList.add("blink");
            }else{
                auto_writing_container.classList.remove("blink");
                pause = Math.ceil(Math.random()*40)+120;
            }
            auto_writing_timeout = setTimeout(function(){
                rewrite(s);
            },pause);
        }else{
            auto_writing_timeout = setTimeout(function(){
                rewrite(s);
            },200);
        }
    }else{
        auto_writing_container.classList.add("blink");
    }
    return;
}