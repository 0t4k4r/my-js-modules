window.addEventListener("load",function(){

});

var left = 0;
var top = 0;

var horizontal_direction = 1;
var vertical_direction = 1;

var amount = 20;

var logo = null;
var logo_width = 0;
var logo_height = 0;

function startScreenSaver(){
    logo = document.querySelector("#screensaver-container #logo");
    logo_width = parseInt(window.getComputedStyle(logo).width.replace("px",""));
    logo_height = parseInt(window.getComputedStyle(logo).height.replace("px",""));
    var screen_saver = setInterval(screenSaver, 17);
}

function screenSaver(){
    if(left == window.innerWidth - logo_width || left == 0){
        horizontal_direction *= -1;
    }
    if(top == window.innerWidth - logo_width || top == 0){
        horizontal_direction *= -1;
    }
    left += horizontal_direction * amount;
    top += vertical_direction * amount;
    left = left < window.innerWidth - logo_width ? left : window.innerWidth - logo_width;
    top = top < window.innerHeight - logo_height ? top : window.innerHeight - logo_height;
    left = left > 0 ? left : 0;
    top = top > 0 ? top : 0;
    logo.style.left = left+"px";
    logo.style.top = top+"px";
}