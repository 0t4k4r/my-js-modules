var otakar_beinhauers_calendar_data_storage = "";

function getDay(y,m,d){
    y = m < 3 ? y - 1 : y;
    m = m == 1 ? 13 : m;
    m = m == 2 ? 14 : m;
    var temp_number = d + 2*m + Math.floor(3*(m+1)/5) + y + Math.floor(y/4) - Math.floor(y/100) + Math.floor(y/400) + 2;
    return (temp_number - 2) % 7;
}
function isItLeapYear(y){
    return y % 4 == 0 && (y % 100 != 0 || y % 400 == 0);
}
function DaysInAMonth(y,m){
    if(m < 1 || m > 12)
        console.error("Error - invalid month was passed: month number "+m+" does not exist.");
    days_in_months = [31,28,31,30,31,30,31,31,30,31,30,31];
    return m == 2 ? (isItLeapYear(y) ? 29 : 28) : days_in_months[m-1];
}

function monthNumberToName(num){
    var month_array = ["January","February","March","April","May","June","July","August","September","October","November","December"];
    return month_array[num];
}

function CustomJSONtoAssociativeArray(json_obj){
    //works only with specific json format!
    var associative_array = [];
    for(var i = 0; i < json_obj.length; i++){
        var node = json_obj[i];
        if(!associative_array)
            associative_array = [];
        if(!associative_array[node.year])
            associative_array[node.year] = [];
        if(!associative_array[node.year][node.month])
            associative_array[node.year][node.month] = [];
        if(!associative_array[node.year][node.month][node.day])
            associative_array[node.year][node.month][node.day] = [node.data];
        else
            associative_array[node.year][node.month][node.day].push(node.data);
    }
    return associative_array;
}

function initialRequestForData(y){
    //Part 2 - getting data from previous, this and next year
    var data = [
        {"year": 2016, "month": 11, "day": 17, "data": 
            [{"hour": 18, "minute": 0, "id": 1, "nazev": "den boje za svobodu a demokracii"}]
        },
        {"year": 2017, "month": 6, "day": 1, "data":
            [{"hour": 0, "minute": 0, "id": 2, "nazev": "den dětí"}]
        },
        {"year": 2017, "month": 11, "day": 17, "data":
            [{"hour": 18, "minute": 0, "id": 3, "nazev": "další den boje za svobodu a demokracii"}]
        },
        {"year": 2017, "month": 12, "day": 13, "data":
            [
                {"hour": 0, "minute": 0, "id": 4, "nazev": "prostě dnešek *EDIT: včerejšek"},
                {"hour": 9, "minute": 31, "id": 5, "nazev": "čas psaní této události"},
                {"hour": 12, "minute": 0, "id": 6, "nazev": "plánované dokončení webu infocentra Hlučín"},
            ]
        },
        {"year": 2017, "month": 12, "day": 14, "data":
            [
                {"hour": 0, "minute": 0, "id": 7, "nazev": "prostě zítřek"},
                {"hour": 15, "minute": 56, "id": 8, "nazev": "psaní TÉTO události (o den dřív)"},
                {"hour": 17, "minute": 30, "id": 9, "nazev": "plánované dokončení webu infocentra Hlučín se trochu natáhne (o den dřív)"},
            ]
        },
        {"year": 2017, "month": 12, "day": 23, "data":
            [{"hour": 10, "minute": 0, "id": 10, "nazev": "jedu na chatu"}]
        },
        {"year": 2017, "month": 12, "day" : 24, "data":
            [{"hour": 18, "minute": 0, "id": 11, "nazev": "štědrovečerní večeře"}]
        },
        {"year": 2018, "month": 1, "day" : 2, "data":
            [{"hour": 17, "minute": 0, "id": 12, "nazev": "přijíždím z chaty domů"}]
        },
        {"year": 2018, "month": 5, "day" : 21, "data":
            [{"hour": 10, "minute": 15, "id": 13, "nazev": "Otakar Beinhauer má narozeniny"}]
        },
    ];
    data = JSON.stringify(data);
    /*
    var xhr = new XMLHttpRequest();
    xhr.open('GET', 'send-ajax-data.php');
    xhr.send(null); //parameter should be the current year
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
            if (xhr.status === 200){
                var obj = JSON.parse(xhr.responseText);*/
                //Part 3 - altering component with data
                if(data)
                    otakar_beinhauers_calendar_data_storage = CustomJSONtoAssociativeArray(JSON.parse(data));
            /*}
        } else {
            console.log('Error: ' + xhr.status);
        }
    }*/
}

function createNewTBodyElement(y, m){
    var firstDay = getDay(y,m,1);
    var daysCount = DaysInAMonth(y,m);
    var table = document.querySelector("#calendar-container table");
    var tbody = document.createElement("tbody");
    tbody.id = "calendar-container-"+y+"-"+m;
    tbody.classList.add("month");
    var html = "<tr>";
    for(var i = 0; i < firstDay; i++){
        html += "<td></td>";
    }
    for(var i = firstDay; i < daysCount + firstDay; i++){
        html += (i+1) % 7 == 0 && i != daysCount + firstDay - 1 ? "<td>"+(i-firstDay+1)+"</td></tr>" : "<td>"+(i-firstDay+1)+"</td>";
    }
    for(var i = daysCount + firstDay - 1; (i+1) % 7 != 0; i++){
        html += "<td></td>";
    }
    html += "</tr>";
    tbody.innerHTML = html;
    table.appendChild(tbody);
}

function activateTBody(y,m){
    if(document.querySelector("#calendar-container tbody.month.active"))
        document.querySelector("#calendar-container tbody.month.active").classList.remove("active");
    document.querySelector("#calendar-container-"+y+"-"+m).classList.add("active");
}

function addDayProgram(y, m, d, program_data){
    var program = document.createElement("div");
    program.id = "calendar-program-"+y+"-"+m+"-"+d;
    program.classList.add("program");
    var html = "";
    html += "<h3>"+d+". "+m+". "+y+"</h3>";
    html += "<table>";
    for(var i = 0; i < program_data.length; i++){
        var event = program_data[i];
        html += "<tr>";
            html += "<th>"+((event.hour < 10) ? ("0"+event.hour) : (event.hour))+":"+(event.minute < 10 ? "0"+event.minute : event.minute)+"</th>";
            html += "<td><a href='/program/"+event.id+"'>"+event.nazev+"</a></td>";
        html += "</tr>";
    }
    html += "</table>";
    program.innerHTML = html;
    document.querySelector("#calendar-program").appendChild(program);
}

function renderData(y, m){
    if(otakar_beinhauers_calendar_data_storage[y]){
        if(otakar_beinhauers_calendar_data_storage[y][m]){
            var firstDay = getDay(y, m, 1);
            for(var index in otakar_beinhauers_calendar_data_storage[y][m]){
                (function (index) {
                    var index = parseInt(index);
                    var day = otakar_beinhauers_calendar_data_storage[y][m][index];
                    if(day[0]){
                        addDayProgram(y, m, index, day[0]);
                        var tbody_cell = document.querySelectorAll("#calendar-container-"+y+"-"+m+" td")[firstDay + index - 1];
                        var span = "<span class='data'>"+tbody_cell.innerHTML+"</span>";
                        tbody_cell.innerHTML = span;
                        tbody_cell.querySelector(".data").onclick = function(){
                            if(document.querySelector("#calendar-container tbody td span.active"))
                                document.querySelector("#calendar-container tbody td span.active").classList.remove("active");
                            this.classList.add("active");
                            if(document.querySelector(".program.active"))
                                document.querySelector(".program.active").classList.remove("active");
                            document.querySelector("#calendar-program-"+y+"-"+m+"-"+index).classList.add("active");
                        };
                    }
                })(index);
            }
        }
    }
}

function renderMonth(y,m){
    y = parseInt(y);
    m = parseInt(m);
    document.querySelector("#calendar-container .month-selector .month").innerHTML = monthNumberToName(m-1);
    document.querySelector("#calendar-container .month-selector .month").dateTime = y + "-" + (m - 1 < 10 ? "0" + (m - 1) : (m - 1));
    document.querySelector("#calendar-container .month-selector .year").innerHTML = y;
    document.querySelector("#calendar-container .month-selector .year").dateTime = y;
    if(!document.querySelector("#calendar-container-"+y+"-"+m)){
        createNewTBodyElement(y, m);
        renderData(y, m);
    }
    activateTBody(y, m);
}

function calendarInit(){
    var date = new Date();
    var y = date.getFullYear();
    var m = date.getMonth() + 1;
    var d = date.getDate();
    var firstDay = getDay(y,m,1);
    initialRequestForData(y);
    renderMonth(y, m);
    var todaysCell = document.querySelectorAll("#calendar-container-"+y+"-"+m+" td")[firstDay + d - 1];
    if(~todaysCell.innerHTML.indexOf("span"))
        todaysCell.querySelector("span").classList.add("today");
    else
        todaysCell.innerHTML = "<span class='today'>"+todaysCell.innerHTML+"</span>";
}

function getThisMonthData(y, m){
    var data = [
        {"year": 2020, "month": 1, "day": 6, "data": 
            [{"hour": 13, "minute": 0, "id": 14, "nazev": "tříkrálová sbírka 2019"}]
        },
        {"year": 2020, "month": 1, "day": 20, "data":
            [
                {"hour": 23, "minute": 58, "id": 15, "nazev": "náhodně vybraný den a čas"},
                {"hour": 23, "minute": 59, "id": 16, "nazev": "náhodně vybraný den a čas č. 2"}
            ]
        },
    ];
    data = JSON.stringify(data);
    /*
    var xhr = new XMLHttpRequest();
    xhr.open('GET', 'send-ajax-data.php');
    xhr.send(null); //parameter should be the current year, and month
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
            if (xhr.status === 200){
                var obj = JSON.parse(xhr.responseText);*/
                //Part 3 - altering component with data
                if(data){
                    var obj = JSON.parse(data);
                    var month_data = CustomJSONtoAssociativeArray(JSON.parse(data));
                    if(month_data[y]){
                        if(month_data[y][m]){
                            for(var index in month_data[y][m]){
                                var d = month_data[y][m][index];
                                if(!otakar_beinhauers_calendar_data_storage[y])
                                    otakar_beinhauers_calendar_data_storage[y] = [];
                                if(!otakar_beinhauers_calendar_data_storage[y][m])
                                    otakar_beinhauers_calendar_data_storage[y][m] = [];
                                otakar_beinhauers_calendar_data_storage[y][m][index] = d;
                            }
                        }
                    }
                }
            /*}
        } else {
            console.log('Error: ' + xhr.status);
        }
    }*/
}

document.body.onload = function(){
    calendarInit();
}

function nextMonth(){
    var date = document.querySelector("#calendar-container tbody.month.active").id.replace("calendar-container-","").split("-");
    var y = parseInt(date[0]);
    var m = parseInt(date[1]);
    if(m == 12){
        y++;
        m = 1;
    }else{
        m++;
    }
    var d = new Date();
    if(y > d.getFullYear()+1){
        getThisMonthData(y, m);
    }
    renderMonth(y, m);
}

function previousMonth(){
    var date = document.querySelector("#calendar-container tbody.month.active").id.replace("calendar-container-","").split("-");
    var y = date[0];
    var m = date[1];
    if(m == 1){
        y--;
        m = 12;
    }else{
        m--;
    }
    var d = new Date();
    if(y < d.getFullYear() - 1){
        getThisMonthData(y, m);
    }
    renderMonth(y, m);
}