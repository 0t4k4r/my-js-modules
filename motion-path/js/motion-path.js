//povots array
var motion_path = {
    "test-01" : [
        {style: "top: 0;left: 0;"},
        {style: "top: 0;right: 0;"},
        {style: "top: 50%;right: 0;transform: translateY(-50%);"},
        {style: "top: 50%;left: 50%;transform: translate(-50%,-50%);"},
        {style: "bottom: 0;left: 50%;transform: translateX(-50%);"},
        {style: "bottom: 0;left: 0;"}]
    };

var amount_of_states = [];
var state_of_items = [];
var carousels_array = [];
var sheets = [];

function initCarousel(){
    carousels = document.querySelectorAll(".carousel");
    for(var carousel_index = 0; carousel_index < carousels.length; carousel_index++){
        var id = carousels[carousel_index].id
        carousels_array[id] = carousels[carousel_index].querySelectorAll(".carousel-item");
        state_of_items[id] = -1;
        amount_of_states[id] = motion_path[id].length;
        console.log(amount_of_states[id]);
        sheets[id] = document.createElement('style');
        sheets[id].id = id+"-stylesheet";
        document.body.appendChild(sheets[id]);
        nextState(id);
    }
    
}
window.addEventListener("load",initCarousel);


function nextState(id){
    console.log(amount_of_states);
    state_of_items[id] = (state_of_items[id] + 1) % amount_of_states[id];
    for(var i = 0; i < amount_of_states[id]; i++){
        var selector = "#"+id+" .carousel-item";
        var properties = motion_path[id][state_of_items[id] + i];
        var rule = selector + "{" + properties + "}";
        document.querySelector("#"+id+"-stylesheet").insertRule(rule,0);
    }
}

function previousState(id){

}