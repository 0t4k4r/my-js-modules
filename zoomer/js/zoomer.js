//Zoomer
window.addEventListener("load",zoomerInit);

var zoomer_top_position = 0;
var zoomer_left_position = 0;
function getCoords(elem) { // crossbrowser version
    var box = elem.getBoundingClientRect();

    var body = document.body;
    var docEl = document.documentElement;

    var scrollTop = window.pageYOffset || docEl.scrollTop || body.scrollTop;
    var scrollLeft = window.pageXOffset || docEl.scrollLeft || body.scrollLeft;

    var clientTop = docEl.clientTop || body.clientTop || 0;
    var clientLeft = docEl.clientLeft || body.clientLeft || 0;

    var top  = box.top +  scrollTop - clientTop;
    var left = box.left + scrollLeft - clientLeft;

    return { top: Math.round(top), left: Math.round(left) };
}
function zoomerInit(){
    var low_quality_image = document.querySelector('#zoomer > img');
    var image_width = window.getComputedStyle(low_quality_image).width.replace("px","");
    var image_height = window.getComputedStyle(low_quality_image).height.replace("px","");
    var zoomed_container = document.querySelector("#zoomed-image-container");
	zoomer_top_position = getCoords(document.querySelector('#zoomer')).top;
    zoomer_left_position = getCoords(document.querySelector('#zoomer')).left;
    console.log("top:"+zoomer_top_position);
    console.log("left:"+zoomer_left_position);
}
var zoomer_active = false;
function setZoomedImageContainerActive(){
	document.querySelector('#zoomed-image-container').classList.add('active')
}
function unsetZoomedImageContainerActive(){
	document.querySelector('#zoomed-image-container').classList.remove('active')
}
function showMagnifyingGlass(){
	if(!zoomer_active){
		document.querySelector("#zoomer").addEventListener("mouseenter",setZoomedImageContainerActive);
		document.querySelector("#zoomer").addEventListener("mouseleave",unsetZoomedImageContainerActive);	
		document.querySelector("#zoomer-button").innerHTML = "<img src='/img/close.svg' alt='exit' /><span class='active'>ZOOM</span>";
		zoomer_active = true;	
	}
	else{
		document.querySelector("#zoomer").removeEventListener("mouseenter",setZoomedImageContainerActive);
		document.querySelector("#zoomer").removeEventListener("mouseleave",unsetZoomedImageContainerActive);
		document.querySelector("#zoomer-button").innerHTML = "<img src='/img/magnifying-glass.svg' alt='lupa' /><span>ZOOM</span>";
		zoomer_active = false;
	}
}

function zoomIn(event){
    var low_quality_image = document.querySelector('#zoomer > img');
    var image_width = window.getComputedStyle(low_quality_image).width.replace("px","");
    var image_height = window.getComputedStyle(low_quality_image).height.replace("px","");
	var container = document.querySelector("#zoomed-image-container");
	var container_height = window.getComputedStyle(container).height.replace("px","");
	var container_width = window.getComputedStyle(container).width.replace("px","");
    var y = event.pageY - zoomer_top_position;
    var x = event.pageX - zoomer_left_position;
    container.style.left = x - container_width/2 + "px";
    container.style.top = y - container_height/2 + "px";
    x_percentage = (x / image_width)*100;
    y_percentage = (y / image_height)*100;
    x_shift = (x)*(container_width/image_width) - container_width/2;
    y_shift = (y)*(container_height/image_height) - container_height/2;
    document.querySelector("#zoomed-image-container").style.backgroundPosition = "calc(" + x_percentage + "% - " + x_shift + "px) " + "calc(" + y_percentage + "% - " + y_shift + "px)";
}