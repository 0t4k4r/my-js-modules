var scroll_ticking = false;

function setupCustomScrollBar(){
    var wrps = document.querySelectorAll("[data-custom-overflow='wrp']");
    for(var i = 0; i < wrps.length; i++){
        var wrp = wrps[i];
        var scrolled = wrp.querySelector(".js-scrolled");
        var scroll_container_width = scrolled.clientWidth;
        var scroll_container_height = scrolled.clientHeight;
        var content = wrp.querySelector("[data-custom-overflow='content']");
        var content_width = parseInt(window.getComputedStyle(content).width.replace("px",""));
        var content_height = parseInt(window.getComputedStyle(content).height.replace("px",""));
        var vertical_scrollbar = wrp.querySelector(".scrollbars .vertical .bar");
        var horizontal_scrollbar = wrp.querySelector(".scrollbars .horizontal .bar");
        vertical_scrollbar.style.height = (scroll_container_height / content_height * 100) + "%";
        horizontal_scrollbar.style.width = (scroll_container_width / content_width * 100) + "%";
        vertical_scrollbar.style.top = (scroll_container_height / content_height) * scrolled.scrollTop + "px";
        horizontal_scrollbar.style.left = (scroll_container_width / content_width) * scrolled.scrollLeft + "px";
        if(scroll_container_width / content_width > 1){
            horizontal_scrollbar.parentNode.style.display = "none";
        }else{
            horizontal_scrollbar.parentNode.style.display = "block";
        }
        if(scroll_container_height / content_height > 1){
            vertical_scrollbar.parentNode.style.display = "none";
        }else{
            vertical_scrollbar.parentNode.style.display = "block";
        }
        wrp.addEventListener("wheel",handleWheel);
    }
}

function doTheScroll(wrp, deltaX, deltaY){
    var scroll_elem = wrp.querySelector(".js-scrolled");
    var vertical_scrollbar = wrp.querySelector(".scrollbars .vertical .bar");
    var horizontal_scrollbar = wrp.querySelector(".scrollbars .horizontal .bar");

    var spaceX = scroll_elem.clientWidth;
    var spaceY = scroll_elem.clientHeight;
    var content_width = parseInt(window.getComputedStyle(wrp.querySelector(".content")).width.replace("px",""));
    var content_height = parseInt(window.getComputedStyle(wrp.querySelector(".content")).height.replace("px",""));
    var potential_scroll_x = content_width - spaceX;
    var potential_scroll_y = content_height - spaceY;

    var horizontal_thumb_width = parseInt(window.getComputedStyle(horizontal_scrollbar).width.replace("px",""));
    var vertical_thumb_height = parseInt(window.getComputedStyle(vertical_scrollbar).height.replace("px",""));
    var horizontal_scrollbar_width = horizontal_scrollbar.parentNode.clientWidth;
    var vertical_scrollbar_height = vertical_scrollbar.parentNode.clientHeight;
    var horizontal_scrollbar_space = horizontal_scrollbar_width - horizontal_thumb_width;
    var vertical_scrollbar_space = vertical_scrollbar_height - vertical_thumb_height;

    var horizontal_ratio = horizontal_scrollbar_space / potential_scroll_x;
    var vertical_ratio = vertical_scrollbar_space / potential_scroll_y;

    if (!scroll_ticking) {
        window.requestAnimationFrame(function() {
            scroll_elem.scrollTop += deltaY;
            scroll_elem.scrollLeft += deltaX;
            vertical_scrollbar.style.top = scroll_elem.scrollTop * vertical_ratio + "px";
            horizontal_scrollbar.style.left = scroll_elem.scrollLeft * horizontal_ratio + "px";
            scroll_ticking = false;
        });
        scroll_ticking = true;
    }
}

function handleWheel(){
    var e = event;
    var wrp = e.currentTarget;
    doTheScroll(wrp, e.deltaX, e.deltaY);
}

var scroll_button_timeout = null;
var scroll_frequency = 100;
var scroll_acceleration = 1;



function buttonScroll(wrp, deltaX, deltaY, direction){
    if(!scroll_button_timeout){
        window.addEventListener("mouseup",cancelButtonScroll);
    }
    doTheScroll(wrp, deltaX, deltaY);
    scroll_button_timeout = setTimeout(function(){
        if(scroll_frequency > scroll_acceleration){
            scroll_frequency -= scroll_acceleration;
            scroll_acceleration += 2;
            deltaX += direction == "right" ? 2 : 0;
            deltaX += direction == "left" ? -2 : 0;
            deltaY += direction == "up" ? -2 : 0;
            deltaY += direction == "down" ? 2 : 0;
        }
        buttonScroll(wrp, deltaX, deltaY);
    },scroll_frequency);
}

function cancelButtonScroll(){
    clearTimeout(scroll_button_timeout);
    scroll_button_timeout = null;
    scroll_frequency = 100;
    scroll_acceleration = 1;
    window.removeEventListener("mouseup",cancelButtonScroll);
}

var scroll_drag_last_position = 0;
var scroll_drag_func_reference = null;

function scrollbarDragBegin(){
    var bar = event.currentTarget;
    var direction = bar.parentNode.classList.contains("vertical") ? "vertical" : "horizontal";
    scroll_drag_last_position = direction == "vertical" ? event.clientY : event.clientX;
    scroll_drag_func_reference = scrollbarDrag.bind(null, bar);
    window.addEventListener("mousemove",scroll_drag_func_reference);
    window.addEventListener("mouseup",scrollbarDragEnd);
}

function scrollbarDrag(bar, event){
    var wrp = bar.parentNode.parentNode.parentNode;
    var scroll_elem = wrp.querySelector(".js-scrolled");
    var direction = bar.parentNode.classList.contains("vertical") ? "vertical" : "horizontal";

    var space = direction == "vertical" ? scroll_elem.clientHeight : scroll_elem.clientWidth;
    var content_size = direction == "vertical" ? parseInt(window.getComputedStyle(wrp.querySelector(".content")).height.replace("px","")) : parseInt(window.getComputedStyle(wrp.querySelector(".content")).width.replace("px",""));
    var potential_scroll = content_size - space;

    var thumb_size = direction == "vertical" ? parseInt(window.getComputedStyle(bar).height.replace("px","")) : parseInt(window.getComputedStyle(bar).width.replace("px",""));
    var scrollbar_size = direction == "vertical" ? bar.parentNode.clientHeight : bar.parentNode.clientWidth;
    var scrollbar_space = scrollbar_size - thumb_size;

    var ratio = scrollbar_space / potential_scroll;

    var mouse_current_position = direction == "vertical" ? event.clientY : event.clientX;
    var mouse_delta = mouse_current_position - scroll_drag_last_position;
    scroll_drag_last_position = mouse_current_position;
    var delta = mouse_delta / ratio;

    var deltaX = direction == "horizontal" ? delta : 0;
    var deltaY = direction == "vertical" ? delta : 0;


    doTheScroll(wrp, deltaX, deltaY);
}

function scrollbarDragEnd(){
    //var bar = event.currentTarget;
    //var direction = bar.parentNode.classList.contains("vertical") ? "vertical" : "horizontal";
    //bar.releasePointerCapture(event.pointerId);
    window.removeEventListener("mousemove",scroll_drag_func_reference);
    window.removeEventListener("mouseup",scrollbarDragEnd);
}

function scrollToMouse(){
    var bar_container = event.currentTarget;
    if(event.target.classList.contains("bar-container")){
        var bar = bar_container.querySelector(".bar");
        var wrp = bar_container.parentNode.parentNode;
        var scroll_elem = wrp.querySelector(".js-scrolled");
        var direction = bar_container.classList.contains("vertical") ? "vertical" : "horizontal";
    
        var space = direction == "vertical" ? scroll_elem.clientHeight : scroll_elem.clientWidth;
        var content_size = direction == "vertical" ? parseInt(window.getComputedStyle(wrp.querySelector(".content")).height.replace("px","")) : parseInt(window.getComputedStyle(wrp.querySelector(".content")).width.replace("px",""));
        var potential_scroll = content_size - space;
    
        var thumb_size = direction == "vertical" ? parseInt(window.getComputedStyle(bar).height.replace("px","")) : parseInt(window.getComputedStyle(bar).width.replace("px",""));
        var thumb_pos = direction == "vertical" ? parseInt(window.getComputedStyle(bar).top.replace("px","")) : parseInt(window.getComputedStyle(bar).left.replace("px",""));
        var scrollbar_size = direction == "vertical" ? bar_container.clientHeight : bar_container.clientWidth;
        var scrollbar_space = scrollbar_size - thumb_size;

        var ratio = scrollbar_space / potential_scroll;

        var rect = bar_container.getBoundingClientRect();
        var cursor_pos = direction == "vertical" ? event.clientY - rect.top - 5 : event.clientX - rect.left - 5;
        
        var scrollbar_delta = cursor_pos < thumb_pos ? cursor_pos - thumb_pos : cursor_pos - (thumb_pos + thumb_size);
        //var scrollbar_delta_percentage = scrollbar_delta / scrollbar_size * 100;
        var delta = scrollbar_delta / ratio;
        var deltaX = direction == "horizontal" ? delta : 0;
        var deltaY = direction == "vertical" ? delta : 0;

        doTheScroll(wrp, deltaX, deltaY);
    }
}

window.addEventListener("load",setupCustomScrollBar);
window.addEventListener("resize",setupCustomScrollBar);